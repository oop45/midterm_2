/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.midterm_2;

/**
 *
 * @author ASUS
 */
public class TestShip {
    public static void main(String[] args) {
        Ship titanic = new Ship(5,6,8);
        titanic.print("Titanic");
        
        Ship unknown = new Ship(6,10,23);
        unknown.print();
        
        Warship yamato = new Warship(20,60,41);
        yamato.print("Yamato");
        
        Warship noname = new Warship(60,70,700);
        noname.print();
    }
}
